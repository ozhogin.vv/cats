import 'package:dio/dio.dart';

class CatsRepository {
  static var headers = {
    "Content-Type": "application/json",
    "x-api-key": "live_QgEzoxNeYvdsnJomwrwxFWMJl7O19jVXg2LklY4S5VZmrTq4InqCWO6D4GSMZWQp",
  };

  var requestOptions = {
    "method": 'GET',
    "headers": headers,
    "redirect": 'follow'
  };

  Future<String> getCats() async {
    final response = await Dio(BaseOptions(responseType: ResponseType.plain)).get(
      "https://api.thecatapi.com/v1/images/search?size=med&mime_types=jpg&format=json&has_breeds=true&order=RANDOM&page=0&limit=100",
      queryParameters: requestOptions,
    );
    return response.toString();
  }
}