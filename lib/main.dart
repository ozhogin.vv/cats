import 'package:cats/screens/likes_screen.dart';
import 'package:flutter/material.dart';
import 'package:cats/screens/splash.dart';
import 'package:cats/screens/home_screen.dart';
import 'package:cats/screens/login_screen.dart';
import 'package:cats/screens/signup_screen.dart';


Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(const CatsTinderApp());
}

class CatsTinderApp extends StatelessWidget {
  const CatsTinderApp({super.key});

  static const authUserKey = 'authUser';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        pageTransitionsTheme: const PageTransitionsTheme(builders: {
          TargetPlatform.android: CupertinoPageTransitionsBuilder(),
        }),
      ),
      routes: {
        '/': (context) => const Splash(),
        '/home': (context) => const HomeScreen(),
        '/login': (context) => const LoginScreen(),
        '/signup': (context) => const SignUpScreen(),
        '/likes': (context) => const LikesScreen(),
      },
      initialRoute: '/',
    );
  }
}