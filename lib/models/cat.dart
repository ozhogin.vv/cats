import 'dart:convert';

List<CatList> catListFromJson(String str) => List<CatList>.from(json.decode(str).map((x) => CatList.fromJson(x)));

String catListToJson(List<CatList> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class CatList {
  String id;
  String url;
  int width;
  int height;

  CatList({
    required this.id,
    required this.url,
    required this.width,
    required this.height,
  });

  factory CatList.fromJson(Map<String, dynamic> json) => CatList(
    id: json["id"],
    url: json["url"],
    width: json["width"],
    height: json["height"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "url": url,
    "width": width,
    "height": height,
  };
}