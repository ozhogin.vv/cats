import 'dart:convert';

User userFromJson(String str) => User.fromJson(json.decode(str));

String userToJson(User data) => json.encode(data.toJson());

class User {
  String email;
  String password;
  List<String> likes;
  int likesCounter;

  User({
    required this.email,
    required this.password,
    required this.likes,
    required this.likesCounter,
  });

  factory User.fromJson(Map<String, dynamic> json) => User(
    email: json["email"],
    password: json["password"],
    likes: List<String>.from(json["likes"].map((x) => x)),
    likesCounter: json["likesCounter"],
  );

  Map<String, dynamic> toJson() => {
    "email": email,
    "password": password,
    "likes": List<dynamic>.from(likes.map((x) => x)),
    "likesCounter": likesCounter,
  };
}