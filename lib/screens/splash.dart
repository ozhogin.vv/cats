import 'package:cats/cubits/internet_cubit.dart';
import 'package:cats/main.dart';
import 'package:cats/models/user.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Splash extends StatefulWidget {
  const Splash({super.key});

  @override
  State<Splash> createState() => _SplashState();
}

class _SplashState extends State<Splash>
  with SingleTickerProviderStateMixin {

  late User? user;

  @override
  void initState() {
    super.initState();
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.immersive);
    initUser();
  }

  @override
  void dispose() {
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: SystemUiOverlay.values);
    super.dispose();
  }

  Future<void> initUser() async {
    var prefs = await SharedPreferences.getInstance();
    var authUserEmail = prefs.getString(CatsTinderApp.authUserKey);
    if (authUserEmail != null) {
      var userJson = prefs.getString(authUserEmail);
      if (userJson != null) {
        user = userFromJson(userJson);
        return;
      }
    }
    user = null;
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<InternetCubit>(
      create: (_) => InternetCubit(connectivity: Connectivity()),
      child: BlocListener<InternetCubit, ConnectivityResult>(
        listener: (context, state) {
          if (state != ConnectivityResult.none) {
            Future.delayed(const Duration(seconds: 2), () {
              if (user != null) {
                Navigator.of(context).pushReplacementNamed(
                    '/home', arguments: user);
              } else {
                Navigator.of(context).pushReplacementNamed(
                    '/login');
              }
            });
          }
        },
        child: Scaffold(
           body: Container (
              width: double.infinity,
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [Colors.pink, Colors.white],
                    begin: Alignment.topRight,
                    end: Alignment.bottomLeft,
                  )
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SvgPicture.asset(
                    'assets/svg/cat_icon.svg',
                    height: 150,
                    width: 150,
                  ),
                  SizedBox(height: 20),
                  BlocBuilder<InternetCubit, ConnectivityResult>(
                      builder: (context, state) {
                        if (state == ConnectivityResult.none) {
                          return Text('Нет интернет соединения', style: TextStyle(
                            fontSize: 28,
                          ),
                          );
                        } else {
                          return Text('Ozhogin Cats', style: TextStyle(
                            fontSize: 32,
                          ),);
                        }
                      },
                  ),
                ],
              ),
           ),
        ),
      ),
    );
  }
}
