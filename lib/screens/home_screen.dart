import 'package:cats/models/cat.dart';
import 'package:cats/models/user.dart';
import 'package:cats/repositories/cats_repository.dart';
import 'package:cats/cubits/cats_cubit.dart';
import 'package:cats/main.dart';
import 'package:cats/services/custom_icons.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';


class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final user = ModalRoute.of(context)?.settings.arguments as User;

    return BlocProvider(
        create: (context) => CatsCubit(CatsRepository()),
        child: CatsScreen(user),
    );
  }
}


class CatsScreen extends StatefulWidget {
  final user;
  const CatsScreen(this.user);

  @override
  State<CatsScreen> createState() => _CatsScreenState();
}

class _CatsScreenState extends State<CatsScreen> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      final cubit = context.read<CatsCubit>();
      cubit.fetchCats();
    });
  }

  Future<void> like(String url) async {
    var prefs = await SharedPreferences.getInstance();
    var likesCounter = widget.user.likesCounter + 1;
    widget.user.likes[likesCounter] = url;
    widget.user.likesCounter = likesCounter;
    var userJson = userToJson(widget.user);
    prefs.setString(widget.user.email, userJson);
  }

  Future<void> signOut() async {
    var prefs = await SharedPreferences.getInstance();
    prefs.remove(CatsTinderApp.authUserKey);
  }

  final ButtonStyle style =
  ElevatedButton.styleFrom(
      padding: EdgeInsets.all(15),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          title: const Text('Ozhogin Cats'),
          actions: [
            IconButton(
              onPressed: () {
                signOut();
                Navigator.of(context).pushNamedAndRemoveUntil('/login', (Route<dynamic> route) => false);
              },
              icon: Icon(Icons.logout),
            ),
          ],
        ),
        body: BlocBuilder<CatsCubit, CatsState>(
          builder: (context, state) {
            return Stack(
              children: [
                Builder(
                  builder: (context) {
                    if (state is CatsInitialState || state is CatsLoadingState) {
                      return const Center(
                        child: CircularProgressIndicator(),
                      );
                    } else if (state is CatsResponseState) {
                      final cats = catListFromJson(state.cats);
                      return Padding(
                        padding: EdgeInsets.all(10),
                        child: Container(
                          constraints: BoxConstraints.expand(height: double.infinity),
                          child: Image.network(cats[state.count].url, fit: BoxFit.cover),
                        ),
                      );
                    } else if (state is CatsErrorState) {
                      return Center(child: Text(state.message));
                    }
                    return Center(child: Text(state.toString()));
                  },
                ),
                Builder(
                    builder: (context) {
                      if (state is CatsResponseState) {
                        final cats = catListFromJson(state.cats);
                        return Container(
                          alignment: AlignmentDirectional.bottomCenter,
                          margin: EdgeInsets.fromLTRB(0, 0, 0, 40),
                          child: Wrap(
                            alignment: WrapAlignment.spaceEvenly,
                            spacing: 40,
                            children: [
                              ElevatedButton(
                                style: style,
                                onPressed: () {
                                  context.read<CatsCubit>().incrementCatsCounter();
                                },
                                child: Icon(CustomIcons.thumbs_down_alt, size: 50, color: Colors.redAccent,),
                              ),
                              ElevatedButton(
                                style: style,
                                onPressed: () {
                                  Navigator.of(context).pushNamed('/likes');
                                },
                                child: Icon(CustomIcons.heart, size: 50, color: Colors.deepPurpleAccent,),
                              ),
                              ElevatedButton(
                                style: style,
                                onPressed: () {
                                  like(cats[state.count].url);
                                  context.read<CatsCubit>().incrementCatsCounter();
                                },
                                child: Icon(CustomIcons.thumbs_up_alt, size: 50, color: Colors.greenAccent,),
                              ),
                            ],
                          ),
                        );
                      }
                      return Center(child: Text(state.toString()));
                    }
                ),
              ],
            );
          },
        ),
    );
  }
}
