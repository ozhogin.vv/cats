part of 'cats_cubit.dart';

@immutable
abstract class CatsState {
  get count => count;
}

class CatsInitialState extends CatsState {}

class CatsLoadingState extends CatsState {}

class CatsResponseState extends CatsState {
  final String cats;
  final int count;
  CatsResponseState(this.cats, this.count);
}

class CatsErrorState extends CatsState {
  final String message;
  CatsErrorState(this.message);
}
