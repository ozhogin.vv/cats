import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:cats/repositories/cats_repository.dart';

part 'cats_state.dart';

class CatsCubit extends Cubit<CatsState> {
  final CatsRepository _catsRepository;
  var _response;
  CatsCubit(this._catsRepository) : super(CatsInitialState());

  Future<void> fetchCats() async {
    emit(CatsLoadingState());
    try {
      _response = await _catsRepository.getCats();
      emit(CatsResponseState(_response, 0));
    } catch (e) {
      emit(CatsErrorState(e.toString()));
    }
  }

  void incrementCatsCounter() {
    if (state.count >= 9) {
      fetchCats();
    }
    emit(CatsResponseState(_response, state.count + 1));
  }
}
